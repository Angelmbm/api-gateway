const usersResolver = {
    Query: {
        userDetailById: (_, { userId }, { dataSources, authData }) => {
            if (userId == authData.userIdToken)
                return dataSources.authAPI.getUser(userId)
            else
                return null
        },
    },

    Mutation: {
        signUpUser: async(_, { userInput }, { dataSources }) => {
            return await dataSources.authAPI.createUser(userInput);
        },

        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.authAPI.authRequest(credentials),

        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.authAPI.refreshToken(refresh),

        updateUser: async(_, {userUpdate}, {dataSources, authData}) => {
            if (userUpdate.id == authData.userIdToken)
                return dataSources.authAPI.updateUser(userUpdate);
            else
                return null
        },

        deleteUser: async(_, {userId}, {dataSources, authData}) => {
            if (userId == authData.userIdToken)
                return dataSources.authAPI.deleteUser(userId);
            else
                return null
        }

    }
};
module.exports = usersResolver;