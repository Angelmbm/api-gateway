const deliveryResolver = {
    Query: {
        deliveryByName: async(_, { name }, {dataSources}) => {
            return await dataSources.inventoryAPI.deliveryByName(name);
        }
    },

    Mutation: {
        createDelivery: async(_, { delivery }, { dataSources }) => {
            return await dataSources.inventoryAPI.createDelivery(delivery);
        }
    }
};
module.exports = deliveryResolver;