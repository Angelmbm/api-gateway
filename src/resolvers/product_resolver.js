const productResolver = {
    Query: {
        products: async(_, {}, {dataSources}) => {
            return await dataSources.inventoryAPI.products();
        },

        productsById: async(_, {id}, {dataSources}) => {
            return await dataSources.inventoryAPI.productsById(id);
        },

        productsByName: async(_, {name}, {dataSources}) => {
            return await dataSources.inventoryAPI.productsByName(name);
        },

        productsByTarget: async(_, {target}, {dataSources}) => {
            return await dataSources.inventoryAPI.productsByTarget(target);
        },

        productsByCategory: async(_, {category}, {dataSources}) => {
            return await dataSources.inventoryAPI.productsByCategory(category);
        },

        productsBySize: async(_, {size}, {dataSources}) => {
            return await dataSources.inventoryAPI.productsBySize(size);
        },

        productById: async(_, {productId}, {dataSources}) => {
            return await dataSources.inventoryAPI.productById(productId);
        }
    },
    Mutation: {
        createProduct: async(_, {product}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.inventoryAPI.createProduct(product);
            else
                return null
        },

        updateProduct: async(_, {product}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.inventoryAPI.updateProduct(product);
            else
                return null
        },

        deleteProduct: async(_, {productId}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.inventoryAPI.deleteProduct(productId);
            else
                return null
        },
    }
};
module.exports = productResolver;