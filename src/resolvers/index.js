const authResolver = require('./auth_resolver');
const productResolver = require('./product_resolver');
const saleResolver = require('./sale_resolver');
const deliveryResolver = require('./delivery_resolver');

const lodash = require('lodash');
const resolvers = lodash.merge(authResolver, productResolver, saleResolver, deliveryResolver);

module.exports = resolvers;