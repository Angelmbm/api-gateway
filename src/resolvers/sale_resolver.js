const saleResolver = {
    Query: {
        sales: async(_, {}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.salesAPI.sales();
            else
                return null
        },

        salesByUser: async(_, {userId}, {dataSources, authData}) => {
            if (authData.userIdToken == userId || authData.userRol != 'user')
                return await dataSources.salesAPI.salesByUser(userId);
            else
                return null
        },

        salesByProduct: async(_, {productId}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.salesAPI.salesByProduct(productId);
            else
                return null
        }, 

        salesByDate: async(_, {date1, date2}, {dataSources, authData}) => {
            if (authData.userRol != 'user')
                return await dataSources.salesAPI.salesByDate(date1, date2);
            else
                return null
        },
    },
    Mutation: {
        createSale: async(_, {sale}, {dataSources, authData}) => {
            if (authData.userIdToken == sale.userId)
                return await dataSources.salesAPI.createSale(sale);
            else
                return null
        }
    }
};
module.exports = saleResolver;