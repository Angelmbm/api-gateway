module.exports = {
    auth_api_url: 'https://dream-shoes-auth-ms.herokuapp.com',
    inventory_api_url: 'https://dream-shoes-inventario-ms.herokuapp.com',
    sales_api_url: 'https://dream-shoes-ventas-ms.herokuapp.com',
};