const { gql } = require('apollo-server');

const saleTypeDefs = gql `
    
    type Sale {
        id: String!
        userId: String
        productId: String
        date: String!
    }

    input SaleInput {
        userId: String!
        productId: String!
    }

    extend type Query {
        sales: [Sale]
        salesByUser(userId: String!): [Sale] 
        salesByProduct(productId: String!): [Sale]
        salesByDate(date1: String!, date2: String!): [Sale]
    }

    extend type Mutation {
        createSale(sale: SaleInput!): Sale
    }
`;
module.exports = saleTypeDefs;