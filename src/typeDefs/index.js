//Se llama al typedef (esquema) de cada submodulo
const authTypeDefs = require('./auth_type_defs');
const productTypeDefs = require('./product_type_defs');
const saleTypeDefs = require('./sale_type_defs');
const deliveryTypeDefs = require('./delivery_type_defs');

//Se unen
const schemasArrays = [authTypeDefs, productTypeDefs, saleTypeDefs, deliveryTypeDefs];
//Se exportan
module.exports = schemasArrays;