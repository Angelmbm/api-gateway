const { gql } = require('apollo-server');

const deliveryTypeDefs = gql `
    type Delivery {
        id: String!
        orderOrigin: String!
        order: String!
        quantity: Int!
        value: Int!
        date: String!
    }

    input OrdersInput {
        orderOrigin: String!
        order: String!
        quantity: Int!
    }

    extend type Query {
        deliveryByName(name: String!): [Delivery]
    }

    extend type Mutation {
        createDelivery(delivery: OrdersInput!): Delivery
    }
`;
module.exports = deliveryTypeDefs;