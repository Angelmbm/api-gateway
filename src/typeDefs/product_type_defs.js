const { gql } = require('apollo-server');

const productTypeDefs = gql `
    
    type Product {
        id: String!
        name: String
        target: String
        category: String
        size: String
        price: Float
        image: String
        inventory: Int
        lastChange: String
    }

    input ProductInput {
        id: String!
        name: String
        target: String
        category: String
        size: String
        price: Int
        image: String
        inventory: Int
        lastChange: String
    }
    
    extend type Query {
        products: [Product]
        productsById(id: String!): [Product]
        productsByName(name: String!): [Product]
        productsByTarget(target: String!): [Product]
        productsByCategory(category: String!): [Product]
        productsBySize(size: String!): [Product]
        productById(productId: String!): Product
    }

    extend type Mutation {
        createProduct(product: ProductInput!): Product
        updateProduct(product: ProductInput!): Product
        deleteProduct(productId: String!): String
    }
`;
module.exports = productTypeDefs;