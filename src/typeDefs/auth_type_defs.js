const { gql } = require('apollo-server');

const authTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    input CredentialsInput {
        email: String!
        password: String!
    }

    input SignUpInput {
        email: String!
        password: String!
        name: String!
        lastName: String!
        Rol: String
    }

    input UserUpdate {
        id: Int!
        password: String
        name: String
        lastName: String
        Rol: String
    }

    type UserDetail {
        id: Int!
        email: String!
        password: String!
        name: String!
        lastName: String!
        Rol: String!
    }

    type Mutation {
        signUpUser(userInput: SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
        updateUser(userUpdate: UserUpdate!): UserDetail!
        deleteUser(userId: Int!): String
    }

    type Query {
        userDetailById(userId: Int!): UserDetail!
    }
`;
module.exports = authTypeDefs;