const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

class SalesAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.sales_api_url;
    }

    async createSale(sale) {
        sale = new Object(JSON.parse(JSON.stringify(sale)));
        return await this.post('/sales', sale);
    }

    async sales() {
        return await this.get('/sales');
    }

    async salesByUser(userId) {
        return await this.get(`/sales/user/${userId}`);
    }

    async salesByProduct(productId) {
        return await this.get(`/sales/product/${productId}`);
    }

    async salesByDate(date1, date2) {
        return await this.get(`/sales/${date1}/${date2}`);
    }

}

module.exports = SalesAPI;