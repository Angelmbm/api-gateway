const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

class InventoryAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.inventory_api_url;
    }

    async createProduct(product) {
        product = new Object(JSON.parse(JSON.stringify(product)));
        return await this.post('/products', product);
    }

    async products() {
        return await this.get('/products');
    }

    async productsById(id) {
        return await this.get(`/products/id/${id}`);
    }

    async productsByName(name) {
        return await this.get(`/products/name/${name}`);
    }

    async productsByTarget(target) {
        return await this.get(`/products/target/${target}`);
    }

    async productsByCategory(category) {
        return await this.get(`/products/category/${category}`);
    }

    async productsBySize(size) {
        return await this.get(`/products/size/${size}`);
    }

    async productById(productId) {
        return await this.get(`/products/id/${productId}`);
    }

    async updateProduct(product) {
        product = new Object(JSON.parse(JSON.stringify(product)));
        return await this.put(`/products/update/${product.id}`, product);
    }

    async deleteProduct(productId) {
        return await this.delete(`/products/delete/${productId}`);
    }

    async createDelivery(delivery) {
        delivery = new Object(JSON.parse(JSON.stringify(delivery)));
        return await this.post('/orders', delivery);
    }

    async deliveryByName(name) {
        return await this.get(`/orders/${name}`);
    }

}

module.exports = InventoryAPI;