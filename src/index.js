const { ApolloServer } = require('apollo-server');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const AuthAPI = require('./dataSources/auth_api');
const InventoryAPI = require('./dataSources/inventory_api');
const SalesAPI = require('./dataSources/sales_api');
const authentication = require('./utils/authentication');


const server = new ApolloServer({
    context: authentication,
    typeDefs,
    resolvers,

    dataSources: () => ({
        authAPI: new AuthAPI(),
        inventoryAPI: new InventoryAPI(),
        salesAPI: new SalesAPI(),
    }),

    introspection: true,
    playground: true
});

server.listen(process.env.PORT || 4000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});